/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author frankkung
 */
public class HTMLParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            Document doc = Jsoup.connect("https://batdongsan.com.vn/ban-can-ho-chung-cu").get();
            System.out.println(doc.title());

            //Get Desc
        } catch (IOException ex) {
            Logger.getLogger(HTMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static final String BASE_URL = "https://batdongsan.com.vn/";

    public static List<Item>  getItems(String uri) {
           List<Item> result = new ArrayList<>();
        try {
         
            Document doc = Jsoup.connect(BASE_URL + uri).get();

            //          get description
            Elements items = doc.getElementsByClass("vip0 search-productItem");
//        System.out.println(items);
            for (Element it : items) {
                Item item = new Item();
                String title = it.select(".p-title").text();
                String url = it.select(".p-title h3 a").attr("href");
                item.setUrl(url);
                item.setTitle(title);
                String image = it.select(".product-avatar img").attr("src");
                item.setImage(image);
                String desc = it.select(".p-main-text").text();
                item.setDescription(desc);
                String price = it.select(".product-price").text();
                item.setPrice(price);
                String area = it.select(".product-area").text();
                String location = it.select(".product-city-dist").text();
                item.setArea(area);
                item.setLocation(location);
                result.add(item);

            }
//        System.out.println(item);
        } catch (IOException ex) {
            Logger.getLogger(HTMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private static void GetImages(Document doc) {
        //          get images link
        Elements text = doc.select("#container > section > article > div.block_td_catalogy > div.main_catalogy > ul > li:nth-child(1) > div > div > div > p.tool_td_catalogy_2 > span:nth-child(1)");
        String desc = text.text();
        System.out.println(desc);
    }

    private static void GetLandDetails(Document doc) {
//         get details of land
        Elements details = doc.select(".div-hold .table-detail .row");
        for (Element e : details) {
            System.out.println(e.select(".left").text() + " | " + e.select(".right").text());

        }
    }

    private static void GetLandLordDetails(Document doc) {
//    owner info
        Elements details = doc.select("#divCustomerInfo .right-content");
        for (Element e : details) {
            System.out.println(e.getElementsByClass("normalblue left").text() + " | " + e.getElementsByClass("right").text());
        }
    }

}
