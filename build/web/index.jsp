<%@page import="org.apache.jasper.tagplugins.jstl.core.Catch"%>
<%@page import="org.jsoup.select.Elements"%>
<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<!DOCTYPE html>

<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
    </head>
    <body id="top">
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row0">
            <div id="topbar" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <div class="fl_left">
                    <ul class="faico clear">
                        <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="faicon-pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="faicon-rss" href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <div class="fl_right">
                    <ul class="nospace inline pushright">
                        <li><i class="fa fa-user"></i> <a href="#">Register</a></li>
                        <li><i class="fa fa-sign-in"></i> <a href="#">Login</a></li>
                    </ul>
                </div>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- Top Background Image Wrapper -->
        <div class="bgded overlay" style="background-image:url('http://www.thuecanho.com/theme/alberlethu2016/images/home/02.png');height: 466px;"> 
            <!-- ################################################################################################ -->
            <div class="wrapper row1">
                <header id="header" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <div id="logo" class="fl_left">
                        <h1><a href="index.html">Etours</a></h1>
                    </div>
                    <nav id="mainav" class="fl_right">
                        <ul class="clear">
                            <li class="active"><a href="index.html">Home</a></li>
                            <li><a class="drop" href="#">Pages</a>
                                <ul>
                                    <li><a href="pages/gallery.html">Gallery</a></li>
                                    <li><a href="pages/full-width.html">Full Width</a></li>
                                    <li><a href="pages/sidebar-left.html">Sidebar Left</a></li>
                                    <li><a href="pages/sidebar-right.html">Sidebar Right</a></li>
                                    <li><a href="pages/basic-grid.html">Basic Grid</a></li>
                                </ul>
                            </li>
                            <li><a class="drop" href="#">Dropdown</a>
                                <ul>
                                    <li><a href="#">Level 2</a></li>
                                    <li><a class="drop" href="#">Level 2 + Drop</a>
                                        <ul>
                                            <li><a href="#">Level 3</a></li>
                                            <li><a href="#">Level 3</a></li>
                                            <li><a href="#">Level 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Level 2</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Link Text</a></li>
                            <li><a href="#">Link Text</a></li>
                        </ul>
                    </nav>
                    <!-- ################################################################################################ -->
                </header>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div id="pageintro" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <article>
                    <h2 class="heading">Vivamus sed ligula tortor quisque</h2>
                    <p>Ut eu viverra sem ultricies bibendum leo ut mollis</p>
                    <footer>
                        <ul class="nospace inline pushright keyword">
                            <li>
                                <input type="text" id="address" placeholder="Quận, Phường, Đường, Dự án hoặc địa danh ..." style="color: black;">
                            </li>
                            <li>

                                <select name="txtCountry" class="select_join">
                                    <option>-- Select Country --</option>
                                    <option value="1">Georgia</option>
                                    <option value="2">Afghanistan</option>
                                </select>


                            </li>
                            <li>

                                <select name="txtCountry" class="select_join">
                                    <option>-- Select Country --</option>
                                    <option value="1">Georgia</option>
                                    <option value="2">Afghanistan</option>
                                </select>

                            </li>
                            <li>

                                <select name="txtCountry" class="select_join">
                                    <option>-- Select Country --</option>
                                    <option value="1">Georgia</option>
                                    <option value="2">Afghanistan</option>
                                </select>

                            </li>
                            </li>
                            <li><a class="btn" href="#">Ullamcorper</a></li>
                        </ul>
                    </footer>
                </article>
                <!-- ################################################################################################ -->
            </div>
            <!-- ################################################################################################ -->
        </div>
        <!-- End Top Background Image Wrapper -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <main class="hoc container clear"> 
                <!-- main body -->
                <!-- main body -->
                <!-- ################################################################################################ -->
                <div class="content three_quarter first"> 
                    <!-- ################################################################################################ -->
                    <div id="gallery">
                        <figure>
                            <header class="heading">
                                <h1>Bán bất động sản, bán nhà đất giá rẻ tại Hà Nội</h1>
                                <div class="page-desciption">
                                    Thông tin rao vặt, mua bán nhà đất, bất động sản giá rẻ, uy tín tại Hà Nội. Hàng ngàn tin đăng mua bán nhà ở, đất trống, căn hộ, mặt bằng cập nhật liên tục 24h tại Mogi.vn Hà Nội
                                </div>
                            </header>

                            <c:import url="${pageContext.request.contextPath}/GetXML?id=${uri}" var="listItem"></c:import>
                                <div class="list-view">
                                <x:parse xml="${listItem}" var="myXML" ></x:parse>
                                <x:forEach select="$myXML/listItem/item" var="item">
                              
                                    <div class="list-view-item clearfix ">
                                        <div class="list-view-image">
                                           
                                            <a href="${pageContext.request.contextPath}/GetDetail?id=<x:out select="$item/url"></x:out>">
                                                <img src="<x:out select="$item/image"></x:out>">
                                            </a>
                                                
                                                <!--                                            <span class="gallery-image-total">
                                                                                                <span><i class="icon icon-image"></i>1</span>
                                                                                                <span><i class="icon icon-clip"></i>0</span>
                                                                                                <span><i class="icon icon-plan"></i>4</span>
                                                                                            </span>-->
                                                <span class="list-view-price">  Giá : <x:out select="$item/price"></x:out></span>
                                            </div>
                                            <div class="list-view-content">
                                                <div class="content">
                                                    <div class="title2">
                                                        <a href="${pageContext.request.contextPath}/GetDetail?id=<x:out select="$item/url"></x:out>">
                                                        <h2><x:out select="$item/title"></x:out></h2>
                                                        </a></div>
                                                    <div class="title1">

                                                        <x:out select="$item/location"></x:out>
                                                    </div>
                                                    <div class="room">
<!--                                                        <span><i class="fa fa-bed"></i> 2</span>
                                                        <span class="bathroom"><i class="fa fa-bath"></i> 2</span>-->
                                                        <span><i class="icon icon-acreage"></i> <x:out select="$item/area"></x:out></span>
                                                    </div>
                                                    <div class="summary">
                                                       
                                                         <x:out select="$item/description" escapeXml="false"></x:out>
                                                    </div>
                                                </div>
                                                <span class="date"></span>
                                                <div class="list-view-agent">
                                                    <div class="agent-content">
                                                        <img src="/content/images/avatar.png" alt="">
                                                        <span class="agent-name"></span>
                                                        <div class="agent-mobile hidden-xs">
                                                        </div>
                                                    </div>

                                                    <div class="favorite "><i id="2451054" class="fa fa-heart"></i></div>

                                                </div>
                                            </div>
                                        </div>
                                </x:forEach>
                            </div>
                            <figcaption>Gallery Description Goes Here</figcaption>
                        </figure>
                    </div>
                    <!-- ################################################################################################ -->
                    <!-- ################################################################################################ -->
                
                    <!-- ################################################################################################ -->
                </div>
                <!-- ################################################################################################ -->
                <!-- ################################################################################################ -->
                <div class="sidebar one_quarter "> 
                    <!-- ################################################################################################ -->
                    <h6>Lorem ipsum dolor</h6>
                    <nav class="sdb_holder">
                        <ul>
                            <li><a href="#">Navigation - Level 1</a></li>
                            <li><a href="${pageContext.request.contextPath}//nha-dat-ban">Nhà Đất bán</a>
                                <ul>
                                    <li><a href="#">Navigation - Level 2</a></li>
                                    <li><a href="#">Navigation - Level 2</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Navigation - Level 1</a>
                                <ul>
                                    <li><a href="#">Navigation - Level 2</a></li>
                                    <li><a href="#">Navigation - Level 2</a>
                                        <ul>
                                            <li><a href="#">Navigation - Level 3</a></li>
                                            <li><a href="#">Navigation - Level 3</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#">Navigation - Level 1</a></li>
                        </ul>
                    </nav>
                    <div class="sdb_holder">
                        <h6>Lorem ipsum dolor</h6>
                        <address>
                            Full Name<br>
                            Address Line 1<br>
                            Address Line 2<br>
                            Town/City<br>
                            Postcode/Zip<br>
                            <br>
                            Tel: xxxx xxxx xxxxxx<br>
                            Email: <a href="#">contact@domain.com</a>
                        </address>
                    </div>
                    <div class="sdb_holder">
                        <article>
                            <h6>Lorem ipsum dolor</h6>
                            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed.</p>
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit</a></li>
                                <li>Etiam vel sapien et</li>
                                <li><a href="#">Etiam vel sapien et</a></li>
                            </ul>
                            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed. Condimentumsantincidunt dui mattis magna intesque purus orci augue lor nibh.</p>
                            <p class="more"><a href="#">Continue Reading »</a></p>
                        </article>
                    </div>
                    <!-- ################################################################################################ -->
                </div>
                <!-- ################################################################################################ -->
                <!-- / main body -->
                <div class="clear"></div>
            </main>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper">
            <section id="shout" class="hoc container clear"> 
                <!-- ################################################################################################ -->
                <div class="one_quarter first"><a class="btn" href="#">Consectetur gravida</a></div>
                <div class="three_quarter">
                    <h2 class="heading btmspace-10">Dolor nec malesuada etiam erat ipsum</h2>
                    <p class="nospace">Nam dictum egestas lacus in iaculis tortor malesuada vitae duis malesuada tortor lacinia erat.</p>
                </div>
                <!-- ################################################################################################ -->
            </section>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper bgded" style="background-image:url('images/demo/backgrounds/02.png');">
            <div class="hoc split clear">
                <article class="box clear"> 
                    <!-- ################################################################################################ -->
                    <h6 class="heading">Facilisis malesuada odio</h6>
                    <p class="btmspace-30">Diam quisque urna odio ornare ac nisl dictum varius venenatis leo nunc magna velit posuere ut mollis ac consequat et lectus fusce pharetra consequat&hellip;</p>
                    <footer><a class="btn inverse" href="#">Read More</a></footer>
                    <!-- ################################################################################################ -->
                </article>
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/03.png');">
            <div class="hoc container clear"> 
                <!-- ################################################################################################ -->
                <div class="testimonial clear center"><img class="circle btmspace-30" src="images/demo/60x60.png" alt="">
                    <blockquote>Rhoncus orci a porttitor nunc elit et neque suspendisse potenti nunc ut dui ut lacus pulvinar pulvinar suspendisse ex quam vehicula et sollicitudin ac efficitur a est donec et rutrum enim cras.</blockquote>
                    <div><strong>A.Doe</strong><br>
                        <em>Job / Title</em></div>
                </div>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <section class="hoc container clear"> 
                <!-- ################################################################################################ -->
                <div class="btmspace-80 center">
                    <h3 class="nospace">Eget vehicula sapien</h3>
                    <p class="nospace">A lacus blandit sodales mi a facilisis quam cras tincidunt.</p>
                </div>
                <div class="group">
                    <article class="one_quarter first">
                        <time class="block font-xs" datetime="2045-04-06">April 6, 2045</time>
                        <h4 class="nospace font-x1">Auctor nulla lectus</h4>
                        <p class="btmspace-30">Feugiat ac praesent dignissim metus ut tellus convallis non varius nulla accumsan&hellip;</p>
                        <a href="#"><img class="btmspace-30" src="images/demo/380x320.png" alt=""></a>
                        <footer><a href="#">Read More &raquo;</a></footer>
                    </article>
                    <article class="one_quarter">
                        <time class="block font-xs" datetime="2045-04-05">April 5, 2045</time>
                        <h4 class="nospace font-x1">Augue congue et</h4>
                        <p class="btmspace-30">Venenatis nec porttitor vel metus phasellus venenatis ex ac bibendum dictum&hellip;</p>
                        <a href="#"><img class="btmspace-30" src="images/demo/380x320.png" alt=""></a>
                        <footer><a href="#">Read More &raquo;</a></footer>
                    </article>
                    <article class="one_quarter">
                        <time class="block font-xs" datetime="2045-04-04">April 4, 2045</time>
                        <h4 class="nospace font-x1">Libero justo luctus</h4>
                        <p class="btmspace-30">Sed congue odio venenatis fusce purus quam lacinia sed sem a viverra mollis&hellip;</p>
                        <a href="#"><img class="btmspace-30" src="images/demo/380x320.png" alt=""></a>
                        <footer><a href="#">Read More &raquo;</a></footer>
                    </article>
                    <article class="one_quarter">
                        <time class="block font-xs" datetime="2045-04-03">April 3, 2045</time>
                        <h4 class="nospace font-x1">Luctus consequat</h4>
                        <p class="btmspace-30">Eget sed blandit eros semper vulputate lorem a efficitur purus aenean leo&hellip;</p>
                        <a href="#"><img class="btmspace-30" src="images/demo/380x320.png" alt=""></a>
                        <footer><a href="#">Read More &raquo;</a></footer>
                    </article>
                </div>
                <!-- ################################################################################################ -->
            </section>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row4">
            <footer id="footer" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <div class="one_quarter first">
                    <h6 class="title">Mauris ut tempus</h6>
                    <address class="btmspace-30">
                        Street Name &amp; Number<br>
                        Town<br>
                        Postcode/Zip
                    </address>
                    <ul class="nospace">
                        <li class="btmspace-10"><i class="fa fa-phone"></i> +00 (123) 456 7890</li>
                        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
                    </ul>
                </div>
                <div class="one_quarter">
                    <h6 class="title">Augue suspendisse</h6>
                    <article>
                        <h2 class="nospace font-x1"><a href="#">Vestibulum augue at</a></h2>
                        <time class="font-xs" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
                        <p>Maximus aliquet neque ac luctus elit praesent imperdiet dui arcu a feugiat tellus interdum ut aliquam.</p>
                    </article>
                </div>
                <div class="one_quarter">
                    <h6 class="title">Sagittis interdum</h6>
                    <ul class="nospace linklist">
                        <li><a href="#">Quis justo luctus sodales</a></li>
                        <li><a href="#">Id commodo enim vivamus</a></li>
                        <li><a href="#">Mattis nisl et interdum</a></li>
                        <li><a href="#">Non urna a eros cursus</a></li>
                        <li><a href="#">Rutrum eleifend posuere</a></li>
                    </ul>
                </div>
                <div class="one_quarter">
                    <h6 class="title">Phasellus ac risus</h6>
                    <p>Venenatis mauris in hendrerit posuere proin lacus massa luctus id iaculis id viverra.</p>
                    <p>Vestibulum nisi nullam vestibulum felis quis fringilla tincidunt nulla dapibus lectus.</p>
                </div>
                <!-- ################################################################################################ -->
            </footer>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row5">
            <div id="copyright" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Domain Name</a></p>
                <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
        <!-- JAVASCRIPTS -->
        <script src="layout/scripts/jquery.min.js"></script>
        <script src="layout/scripts/jquery.backtotop.js"></script>
        <script src="layout/scripts/jquery.mobilemenu.js"></script>
    </body>
</html>