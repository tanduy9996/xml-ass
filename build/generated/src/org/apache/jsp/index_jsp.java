package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.tagplugins.jstl.core.Catch;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_import_var_url_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_x_out_select_escapeXml_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_x_forEach_var_select;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_x_out_select_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_x_parse_xml_var_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_import_var_url_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_set_var_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_x_out_select_escapeXml_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_x_forEach_var_select = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_x_out_select_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_x_parse_xml_var_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_import_var_url_nobody.release();
    _jspx_tagPool_c_set_var_value_nobody.release();
    _jspx_tagPool_x_out_select_escapeXml_nobody.release();
    _jspx_tagPool_x_forEach_var_select.release();
    _jspx_tagPool_x_out_select_nobody.release();
    _jspx_tagPool_x_parse_xml_var_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${title}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</title>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\n");
      out.write("        <link href=\"layout/styles/layout.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">\n");
      out.write("    </head>\n");
      out.write("    <body id=\"top\">\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper row0\">\n");
      out.write("            <div id=\"topbar\" class=\"hoc clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"fl_left\">\n");
      out.write("                    <ul class=\"faico clear\">\n");
      out.write("                        <li><a class=\"faicon-facebook\" href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-pinterest\" href=\"#\"><i class=\"fa fa-pinterest\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-twitter\" href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-dribble\" href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-linkedin\" href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-google-plus\" href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\n");
      out.write("                        <li><a class=\"faicon-rss\" href=\"#\"><i class=\"fa fa-rss\"></i></a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"fl_right\">\n");
      out.write("                    <ul class=\"nospace inline pushright\">\n");
      out.write("                        <li><i class=\"fa fa-user\"></i> <a href=\"#\">Register</a></li>\n");
      out.write("                        <li><i class=\"fa fa-sign-in\"></i> <a href=\"#\">Login</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- Top Background Image Wrapper -->\n");
      out.write("        <div class=\"bgded overlay\" style=\"background-image:url('http://www.thuecanho.com/theme/alberlethu2016/images/home/02.png');height: 466px;\"> \n");
      out.write("            <!-- ################################################################################################ -->\n");
      out.write("            <div class=\"wrapper row1\">\n");
      out.write("                <header id=\"header\" class=\"hoc clear\"> \n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <div id=\"logo\" class=\"fl_left\">\n");
      out.write("                        <h1><a href=\"index.html\">Etours</a></h1>\n");
      out.write("                    </div>\n");
      out.write("                    <nav id=\"mainav\" class=\"fl_right\">\n");
      out.write("                        <ul class=\"clear\">\n");
      out.write("                            <li class=\"active\"><a href=\"index.html\">Home</a></li>\n");
      out.write("                            <li><a class=\"drop\" href=\"#\">Pages</a>\n");
      out.write("                                <ul>\n");
      out.write("                                    <li><a href=\"pages/gallery.html\">Gallery</a></li>\n");
      out.write("                                    <li><a href=\"pages/full-width.html\">Full Width</a></li>\n");
      out.write("                                    <li><a href=\"pages/sidebar-left.html\">Sidebar Left</a></li>\n");
      out.write("                                    <li><a href=\"pages/sidebar-right.html\">Sidebar Right</a></li>\n");
      out.write("                                    <li><a href=\"pages/basic-grid.html\">Basic Grid</a></li>\n");
      out.write("                                </ul>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a class=\"drop\" href=\"#\">Dropdown</a>\n");
      out.write("                                <ul>\n");
      out.write("                                    <li><a href=\"#\">Level 2</a></li>\n");
      out.write("                                    <li><a class=\"drop\" href=\"#\">Level 2 + Drop</a>\n");
      out.write("                                        <ul>\n");
      out.write("                                            <li><a href=\"#\">Level 3</a></li>\n");
      out.write("                                            <li><a href=\"#\">Level 3</a></li>\n");
      out.write("                                            <li><a href=\"#\">Level 3</a></li>\n");
      out.write("                                        </ul>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li><a href=\"#\">Level 2</a></li>\n");
      out.write("                                </ul>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"#\">Link Text</a></li>\n");
      out.write("                            <li><a href=\"#\">Link Text</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </nav>\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                </header>\n");
      out.write("            </div>\n");
      out.write("            <!-- ################################################################################################ -->\n");
      out.write("            <!-- ################################################################################################ -->\n");
      out.write("            <!-- ################################################################################################ -->\n");
      out.write("            <div id=\"pageintro\" class=\"hoc clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <article>\n");
      out.write("                    <h2 class=\"heading\">Vivamus sed ligula tortor quisque</h2>\n");
      out.write("                    <p>Ut eu viverra sem ultricies bibendum leo ut mollis</p>\n");
      out.write("                    <footer>\n");
      out.write("                        <ul class=\"nospace inline pushright keyword\">\n");
      out.write("                            <li>\n");
      out.write("                                <input type=\"text\" id=\"address\" placeholder=\"Quận, Phường, Đường, Dự án hoặc địa danh ...\" style=\"color: black;\">\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("\n");
      out.write("                                <select name=\"txtCountry\" class=\"select_join\">\n");
      out.write("                                    <option>-- Select Country --</option>\n");
      out.write("                                    <option value=\"1\">Georgia</option>\n");
      out.write("                                    <option value=\"2\">Afghanistan</option>\n");
      out.write("                                </select>\n");
      out.write("\n");
      out.write("\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("\n");
      out.write("                                <select name=\"txtCountry\" class=\"select_join\">\n");
      out.write("                                    <option>-- Select Country --</option>\n");
      out.write("                                    <option value=\"1\">Georgia</option>\n");
      out.write("                                    <option value=\"2\">Afghanistan</option>\n");
      out.write("                                </select>\n");
      out.write("\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("\n");
      out.write("                                <select name=\"txtCountry\" class=\"select_join\">\n");
      out.write("                                    <option>-- Select Country --</option>\n");
      out.write("                                    <option value=\"1\">Georgia</option>\n");
      out.write("                                    <option value=\"2\">Afghanistan</option>\n");
      out.write("                                </select>\n");
      out.write("\n");
      out.write("                            </li>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a class=\"btn\" href=\"#\">Ullamcorper</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </footer>\n");
      out.write("                </article>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </div>\n");
      out.write("            <!-- ################################################################################################ -->\n");
      out.write("        </div>\n");
      out.write("        <!-- End Top Background Image Wrapper -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper row3\">\n");
      out.write("            <main class=\"hoc container clear\"> \n");
      out.write("                <!-- main body -->\n");
      out.write("                <!-- main body -->\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"content three_quarter first\"> \n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <div id=\"gallery\">\n");
      out.write("                        <figure>\n");
      out.write("                            <header class=\"heading\">\n");
      out.write("                                <h1>Bán bất động sản, bán nhà đất giá rẻ tại Hà Nội</h1>\n");
      out.write("                                <div class=\"page-desciption\">\n");
      out.write("                                    Thông tin rao vặt, mua bán nhà đất, bất động sản giá rẻ, uy tín tại Hà Nội. Hàng ngàn tin đăng mua bán nhà ở, đất trống, căn hộ, mặt bằng cập nhật liên tục 24h tại Mogi.vn Hà Nội\n");
      out.write("                                </div>\n");
      out.write("                            </header>\n");
      out.write("\n");
      out.write("                            ");

                                Document doc = Jsoup.connect("http://sbatdongsan.com/rss/ban-biet-thu.htm").get();
                                System.out.println(doc.title());
                            
      out.write("\n");
      out.write("                        \n");
      out.write("                            ");
      if (_jspx_meth_c_import_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                <div class=\"list-view\">\n");
      out.write("                                ");
      if (_jspx_meth_x_parse_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                ");
      if (_jspx_meth_x_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                            <figcaption>Gallery Description Goes Here</figcaption>\n");
      out.write("                        </figure>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <nav class=\"pagination\">\n");
      out.write("                        <ul>\n");
      out.write("                            <li><a href=\"#\">&laquo; Previous</a></li>\n");
      out.write("                            <li><a href=\"#\">1</a></li>\n");
      out.write("                            <li><a href=\"#\">2</a></li>\n");
      out.write("                            <li><strong>&hellip;</strong></li>\n");
      out.write("                            <li><a href=\"#\">6</a></li>\n");
      out.write("                            <li class=\"current\"><strong>7</strong></li>\n");
      out.write("                            <li><a href=\"#\">8</a></li>\n");
      out.write("                            <li><a href=\"#\">9</a></li>\n");
      out.write("                            <li><strong>&hellip;</strong></li>\n");
      out.write("                            <li><a href=\"#\">14</a></li>\n");
      out.write("                            <li><a href=\"#\">15</a></li>\n");
      out.write("                            <li><a href=\"#\">Next &raquo;</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </nav>\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"sidebar one_quarter \"> \n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <h6>Lorem ipsum dolor</h6>\n");
      out.write("                    <nav class=\"sdb_holder\">\n");
      out.write("                        <ul>\n");
      out.write("                            <li><a href=\"#\">Navigation - Level 1</a></li>\n");
      out.write("                            <li><a href=\"#\">Navigation - Level 1</a>\n");
      out.write("                                <ul>\n");
      out.write("                                    <li><a href=\"#\">Navigation - Level 2</a></li>\n");
      out.write("                                    <li><a href=\"#\">Navigation - Level 2</a></li>\n");
      out.write("                                </ul>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"#\">Navigation - Level 1</a>\n");
      out.write("                                <ul>\n");
      out.write("                                    <li><a href=\"#\">Navigation - Level 2</a></li>\n");
      out.write("                                    <li><a href=\"#\">Navigation - Level 2</a>\n");
      out.write("                                        <ul>\n");
      out.write("                                            <li><a href=\"#\">Navigation - Level 3</a></li>\n");
      out.write("                                            <li><a href=\"#\">Navigation - Level 3</a></li>\n");
      out.write("                                        </ul>\n");
      out.write("                                    </li>\n");
      out.write("                                </ul>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"#\">Navigation - Level 1</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </nav>\n");
      out.write("                    <div class=\"sdb_holder\">\n");
      out.write("                        <h6>Lorem ipsum dolor</h6>\n");
      out.write("                        <address>\n");
      out.write("                            Full Name<br>\n");
      out.write("                            Address Line 1<br>\n");
      out.write("                            Address Line 2<br>\n");
      out.write("                            Town/City<br>\n");
      out.write("                            Postcode/Zip<br>\n");
      out.write("                            <br>\n");
      out.write("                            Tel: xxxx xxxx xxxxxx<br>\n");
      out.write("                            Email: <a href=\"#\">contact@domain.com</a>\n");
      out.write("                        </address>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"sdb_holder\">\n");
      out.write("                        <article>\n");
      out.write("                            <h6>Lorem ipsum dolor</h6>\n");
      out.write("                            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed.</p>\n");
      out.write("                            <ul>\n");
      out.write("                                <li><a href=\"#\">Lorem ipsum dolor sit</a></li>\n");
      out.write("                                <li>Etiam vel sapien et</li>\n");
      out.write("                                <li><a href=\"#\">Etiam vel sapien et</a></li>\n");
      out.write("                            </ul>\n");
      out.write("                            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed. Condimentumsantincidunt dui mattis magna intesque purus orci augue lor nibh.</p>\n");
      out.write("                            <p class=\"more\"><a href=\"#\">Continue Reading »</a></p>\n");
      out.write("                        </article>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <!-- / main body -->\n");
      out.write("                <div class=\"clear\"></div>\n");
      out.write("            </main>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper\">\n");
      out.write("            <section id=\"shout\" class=\"hoc container clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"one_quarter first\"><a class=\"btn\" href=\"#\">Consectetur gravida</a></div>\n");
      out.write("                <div class=\"three_quarter\">\n");
      out.write("                    <h2 class=\"heading btmspace-10\">Dolor nec malesuada etiam erat ipsum</h2>\n");
      out.write("                    <p class=\"nospace\">Nam dictum egestas lacus in iaculis tortor malesuada vitae duis malesuada tortor lacinia erat.</p>\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </section>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper bgded\" style=\"background-image:url('images/demo/backgrounds/02.png');\">\n");
      out.write("            <div class=\"hoc split clear\">\n");
      out.write("                <article class=\"box clear\"> \n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                    <h6 class=\"heading\">Facilisis malesuada odio</h6>\n");
      out.write("                    <p class=\"btmspace-30\">Diam quisque urna odio ornare ac nisl dictum varius venenatis leo nunc magna velit posuere ut mollis ac consequat et lectus fusce pharetra consequat&hellip;</p>\n");
      out.write("                    <footer><a class=\"btn inverse\" href=\"#\">Read More</a></footer>\n");
      out.write("                    <!-- ################################################################################################ -->\n");
      out.write("                </article>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper bgded overlay\" style=\"background-image:url('images/demo/backgrounds/03.png');\">\n");
      out.write("            <div class=\"hoc container clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"testimonial clear center\"><img class=\"circle btmspace-30\" src=\"images/demo/60x60.png\" alt=\"\">\n");
      out.write("                    <blockquote>Rhoncus orci a porttitor nunc elit et neque suspendisse potenti nunc ut dui ut lacus pulvinar pulvinar suspendisse ex quam vehicula et sollicitudin ac efficitur a est donec et rutrum enim cras.</blockquote>\n");
      out.write("                    <div><strong>A.Doe</strong><br>\n");
      out.write("                        <em>Job / Title</em></div>\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper row3\">\n");
      out.write("            <section class=\"hoc container clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"btmspace-80 center\">\n");
      out.write("                    <h3 class=\"nospace\">Eget vehicula sapien</h3>\n");
      out.write("                    <p class=\"nospace\">A lacus blandit sodales mi a facilisis quam cras tincidunt.</p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"group\">\n");
      out.write("                    <article class=\"one_quarter first\">\n");
      out.write("                        <time class=\"block font-xs\" datetime=\"2045-04-06\">April 6, 2045</time>\n");
      out.write("                        <h4 class=\"nospace font-x1\">Auctor nulla lectus</h4>\n");
      out.write("                        <p class=\"btmspace-30\">Feugiat ac praesent dignissim metus ut tellus convallis non varius nulla accumsan&hellip;</p>\n");
      out.write("                        <a href=\"#\"><img class=\"btmspace-30\" src=\"images/demo/380x320.png\" alt=\"\"></a>\n");
      out.write("                        <footer><a href=\"#\">Read More &raquo;</a></footer>\n");
      out.write("                    </article>\n");
      out.write("                    <article class=\"one_quarter\">\n");
      out.write("                        <time class=\"block font-xs\" datetime=\"2045-04-05\">April 5, 2045</time>\n");
      out.write("                        <h4 class=\"nospace font-x1\">Augue congue et</h4>\n");
      out.write("                        <p class=\"btmspace-30\">Venenatis nec porttitor vel metus phasellus venenatis ex ac bibendum dictum&hellip;</p>\n");
      out.write("                        <a href=\"#\"><img class=\"btmspace-30\" src=\"images/demo/380x320.png\" alt=\"\"></a>\n");
      out.write("                        <footer><a href=\"#\">Read More &raquo;</a></footer>\n");
      out.write("                    </article>\n");
      out.write("                    <article class=\"one_quarter\">\n");
      out.write("                        <time class=\"block font-xs\" datetime=\"2045-04-04\">April 4, 2045</time>\n");
      out.write("                        <h4 class=\"nospace font-x1\">Libero justo luctus</h4>\n");
      out.write("                        <p class=\"btmspace-30\">Sed congue odio venenatis fusce purus quam lacinia sed sem a viverra mollis&hellip;</p>\n");
      out.write("                        <a href=\"#\"><img class=\"btmspace-30\" src=\"images/demo/380x320.png\" alt=\"\"></a>\n");
      out.write("                        <footer><a href=\"#\">Read More &raquo;</a></footer>\n");
      out.write("                    </article>\n");
      out.write("                    <article class=\"one_quarter\">\n");
      out.write("                        <time class=\"block font-xs\" datetime=\"2045-04-03\">April 3, 2045</time>\n");
      out.write("                        <h4 class=\"nospace font-x1\">Luctus consequat</h4>\n");
      out.write("                        <p class=\"btmspace-30\">Eget sed blandit eros semper vulputate lorem a efficitur purus aenean leo&hellip;</p>\n");
      out.write("                        <a href=\"#\"><img class=\"btmspace-30\" src=\"images/demo/380x320.png\" alt=\"\"></a>\n");
      out.write("                        <footer><a href=\"#\">Read More &raquo;</a></footer>\n");
      out.write("                    </article>\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </section>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper row4\">\n");
      out.write("            <footer id=\"footer\" class=\"hoc clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <div class=\"one_quarter first\">\n");
      out.write("                    <h6 class=\"title\">Mauris ut tempus</h6>\n");
      out.write("                    <address class=\"btmspace-30\">\n");
      out.write("                        Street Name &amp; Number<br>\n");
      out.write("                        Town<br>\n");
      out.write("                        Postcode/Zip\n");
      out.write("                    </address>\n");
      out.write("                    <ul class=\"nospace\">\n");
      out.write("                        <li class=\"btmspace-10\"><i class=\"fa fa-phone\"></i> +00 (123) 456 7890</li>\n");
      out.write("                        <li><i class=\"fa fa-envelope-o\"></i> info@domain.com</li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"one_quarter\">\n");
      out.write("                    <h6 class=\"title\">Augue suspendisse</h6>\n");
      out.write("                    <article>\n");
      out.write("                        <h2 class=\"nospace font-x1\"><a href=\"#\">Vestibulum augue at</a></h2>\n");
      out.write("                        <time class=\"font-xs\" datetime=\"2045-04-06\">Friday, 6<sup>th</sup> April 2045</time>\n");
      out.write("                        <p>Maximus aliquet neque ac luctus elit praesent imperdiet dui arcu a feugiat tellus interdum ut aliquam.</p>\n");
      out.write("                    </article>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"one_quarter\">\n");
      out.write("                    <h6 class=\"title\">Sagittis interdum</h6>\n");
      out.write("                    <ul class=\"nospace linklist\">\n");
      out.write("                        <li><a href=\"#\">Quis justo luctus sodales</a></li>\n");
      out.write("                        <li><a href=\"#\">Id commodo enim vivamus</a></li>\n");
      out.write("                        <li><a href=\"#\">Mattis nisl et interdum</a></li>\n");
      out.write("                        <li><a href=\"#\">Non urna a eros cursus</a></li>\n");
      out.write("                        <li><a href=\"#\">Rutrum eleifend posuere</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"one_quarter\">\n");
      out.write("                    <h6 class=\"title\">Phasellus ac risus</h6>\n");
      out.write("                    <p>Venenatis mauris in hendrerit posuere proin lacus massa luctus id iaculis id viverra.</p>\n");
      out.write("                    <p>Vestibulum nisi nullam vestibulum felis quis fringilla tincidunt nulla dapibus lectus.</p>\n");
      out.write("                </div>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </footer>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <div class=\"wrapper row5\">\n");
      out.write("            <div id=\"copyright\" class=\"hoc clear\"> \n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("                <p class=\"fl_left\">Copyright &copy; 2016 - All Rights Reserved - <a href=\"#\">Domain Name</a></p>\n");
      out.write("                <p class=\"fl_right\">Template by <a target=\"_blank\" href=\"http://www.os-templates.com/\" title=\"Free Website Templates\">OS Templates</a></p>\n");
      out.write("                <!-- ################################################################################################ -->\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <!-- ################################################################################################ -->\n");
      out.write("        <a id=\"backtotop\" href=\"#top\"><i class=\"fa fa-chevron-up\"></i></a>\n");
      out.write("        <!-- JAVASCRIPTS -->\n");
      out.write("        <script src=\"layout/scripts/jquery.min.js\"></script>\n");
      out.write("        <script src=\"layout/scripts/jquery.backtotop.js\"></script>\n");
      out.write("        <script src=\"layout/scripts/jquery.mobilemenu.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_import_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:import
    org.apache.taglibs.standard.tag.rt.core.ImportTag _jspx_th_c_import_0 = (org.apache.taglibs.standard.tag.rt.core.ImportTag) _jspx_tagPool_c_import_var_url_nobody.get(org.apache.taglibs.standard.tag.rt.core.ImportTag.class);
    _jspx_th_c_import_0.setPageContext(_jspx_page_context);
    _jspx_th_c_import_0.setParent(null);
    _jspx_th_c_import_0.setUrl("http://sbatdongsan.com/rss/ban-biet-thu.htm");
    _jspx_th_c_import_0.setVar("listItem");
    int[] _jspx_push_body_count_c_import_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_import_0 = _jspx_th_c_import_0.doStartTag();
      if (_jspx_th_c_import_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_import_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_import_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_import_0.doFinally();
      _jspx_tagPool_c_import_var_url_nobody.reuse(_jspx_th_c_import_0);
    }
    return false;
  }

  private boolean _jspx_meth_x_parse_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  x:parse
    org.apache.taglibs.standard.tag.rt.xml.ParseTag _jspx_th_x_parse_0 = (org.apache.taglibs.standard.tag.rt.xml.ParseTag) _jspx_tagPool_x_parse_xml_var_nobody.get(org.apache.taglibs.standard.tag.rt.xml.ParseTag.class);
    _jspx_th_x_parse_0.setPageContext(_jspx_page_context);
    _jspx_th_x_parse_0.setParent(null);
    _jspx_th_x_parse_0.setXml((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${listItem}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_x_parse_0.setVar("myXML");
    int _jspx_eval_x_parse_0 = _jspx_th_x_parse_0.doStartTag();
    if (_jspx_th_x_parse_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_x_parse_xml_var_nobody.reuse(_jspx_th_x_parse_0);
      return true;
    }
    _jspx_tagPool_x_parse_xml_var_nobody.reuse(_jspx_th_x_parse_0);
    return false;
  }

  private boolean _jspx_meth_x_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  x:forEach
    org.apache.taglibs.standard.tag.common.xml.ForEachTag _jspx_th_x_forEach_0 = (org.apache.taglibs.standard.tag.common.xml.ForEachTag) _jspx_tagPool_x_forEach_var_select.get(org.apache.taglibs.standard.tag.common.xml.ForEachTag.class);
    _jspx_th_x_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_x_forEach_0.setParent(null);
    _jspx_th_x_forEach_0.setSelect("$myXML/rss/channel/item");
    _jspx_th_x_forEach_0.setVar("item");
    int[] _jspx_push_body_count_x_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_x_forEach_0 = _jspx_th_x_forEach_0.doStartTag();
      if (_jspx_eval_x_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                    ");
          if (_jspx_meth_c_set_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_x_forEach_0, _jspx_page_context, _jspx_push_body_count_x_forEach_0))
            return true;
          out.write("\n");
          out.write("                                    <div class=\"list-view-item clearfix \">\n");
          out.write("                                        <div class=\"list-view-image\">\n");
          out.write("                                            ");
          if (_jspx_meth_x_out_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_x_forEach_0, _jspx_page_context, _jspx_push_body_count_x_forEach_0))
            return true;
          out.write("\n");
          out.write("                                                <!--<img src=\"https://cdn.mogi.vn/upload/images/thumb-small/201803/19/424/ca010460b67a4109bb39ab42a3b4de18.jpg\">-->\n");
          out.write("                                                <!--                                            <span class=\"gallery-image-total\">\n");
          out.write("                                                                                                <span><i class=\"icon icon-image\"></i>1</span>\n");
          out.write("                                                                                                <span><i class=\"icon icon-clip\"></i>0</span>\n");
          out.write("                                                                                                <span><i class=\"icon icon-plan\"></i>4</span>\n");
          out.write("                                                                                            </span>-->\n");
          out.write("                                                <!--<span class=\"list-view-price\" style=\"margin-top: -87px;\">2 tỷ 580 triệu</span>-->\n");
          out.write("                                            </div>\n");
          out.write("                                            <div class=\"list-view-content\">\n");
          out.write("                                                <div class=\"content\">\n");
          out.write("                                                    <div class=\"title2\">\n");
          out.write("                                                        <a href=\"");
          if (_jspx_meth_x_out_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_x_forEach_0, _jspx_page_context, _jspx_push_body_count_x_forEach_0))
            return true;
          out.write("\">\n");
          out.write("                                                        <h2>");
          if (_jspx_meth_x_out_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_x_forEach_0, _jspx_page_context, _jspx_push_body_count_x_forEach_0))
            return true;
          out.write("</h2>\n");
          out.write("                                                        </a></div>\n");
          out.write("                                                    <div class=\"title1\">\n");
          out.write("\n");
          out.write("                                                        Lê Văn Lương, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội\n");
          out.write("                                                    </div>\n");
          out.write("                                                    <div class=\"room\">\n");
          out.write("                                                        <span><i class=\"fa fa-bed\"></i> 2</span>\n");
          out.write("                                                        <span class=\"bathroom\"><i class=\"fa fa-bath\"></i> 2</span>\n");
          out.write("                                                        <span><i class=\"icon icon-acreage\"></i> 103 m<sup>2</sup></span>\n");
          out.write("                                                    </div>\n");
          out.write("                                                    <div class=\"summary\">\n");
          out.write("\n");
          out.write("                                                        Tôi có mua đầu tư căn hộ chung cư Mỹ đức Complex, do làm ăn thua lỗ nên muốn bán lại cho người mua thiện chí. Căn tầng A0602, DT: 103m2. Thiết kế 3PN, 2WC, view công viên hồ điều hòa Nhân Chính, hướng ban công Đông Nam thoáng mát. Tôi bán giá 23,8\n");
          out.write("                                                    </div>\n");
          out.write("                                                </div>\n");
          out.write("                                                <span class=\"date\">Ngày đăng: Hôm nay</span>\n");
          out.write("                                                <div class=\"list-view-agent\">\n");
          out.write("                                                    <div class=\"agent-content\">\n");
          out.write("                                                        <img src=\"/content/images/avatar.png\" alt=\"\">\n");
          out.write("                                                        <span class=\"agent-name\">2 tỷ 580 triệu</span>\n");
          out.write("                                                        <div class=\"agent-mobile hidden-xs\">\n");
          out.write("                                                            <a gtm-event=\"link\" gtm-cat=\"listing\" gtm-act=\"mobile-show\" href=\"javascript:void(0)\" ng-click=\"Phone.ShowPhone('01657206196')\" ng-bind=\"Phone.GetPhoneNo('01657206196')\" class=\"ng-binding\">016572061xx</a>\n");
          out.write("                                                        </div>\n");
          out.write("                                                    </div>\n");
          out.write("\n");
          out.write("                                                    <div class=\"favorite \"><i id=\"2451054\" class=\"fa fa-heart\"></i></div>\n");
          out.write("\n");
          out.write("                                                </div>\n");
          out.write("                                            </div>\n");
          out.write("                                        </div>\n");
          out.write("                                ");
          int evalDoAfterBody = _jspx_th_x_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_x_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_x_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_x_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_x_forEach_0.doFinally();
      _jspx_tagPool_x_forEach_var_select.reuse(_jspx_th_x_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_set_0(javax.servlet.jsp.tagext.JspTag _jspx_th_x_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_x_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_x_forEach_0);
    _jspx_th_c_set_0.setVar("string1");
    _jspx_th_c_set_0.setValue(new String("$item/description"));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_x_out_0(javax.servlet.jsp.tagext.JspTag _jspx_th_x_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_x_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  x:out
    org.apache.taglibs.standard.tag.rt.xml.ExprTag _jspx_th_x_out_0 = (org.apache.taglibs.standard.tag.rt.xml.ExprTag) _jspx_tagPool_x_out_select_escapeXml_nobody.get(org.apache.taglibs.standard.tag.rt.xml.ExprTag.class);
    _jspx_th_x_out_0.setPageContext(_jspx_page_context);
    _jspx_th_x_out_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_x_forEach_0);
    _jspx_th_x_out_0.setSelect("$item/description");
    _jspx_th_x_out_0.setEscapeXml(false);
    int _jspx_eval_x_out_0 = _jspx_th_x_out_0.doStartTag();
    if (_jspx_th_x_out_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_x_out_select_escapeXml_nobody.reuse(_jspx_th_x_out_0);
      return true;
    }
    _jspx_tagPool_x_out_select_escapeXml_nobody.reuse(_jspx_th_x_out_0);
    return false;
  }

  private boolean _jspx_meth_x_out_1(javax.servlet.jsp.tagext.JspTag _jspx_th_x_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_x_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  x:out
    org.apache.taglibs.standard.tag.rt.xml.ExprTag _jspx_th_x_out_1 = (org.apache.taglibs.standard.tag.rt.xml.ExprTag) _jspx_tagPool_x_out_select_nobody.get(org.apache.taglibs.standard.tag.rt.xml.ExprTag.class);
    _jspx_th_x_out_1.setPageContext(_jspx_page_context);
    _jspx_th_x_out_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_x_forEach_0);
    _jspx_th_x_out_1.setSelect("$item/link");
    int _jspx_eval_x_out_1 = _jspx_th_x_out_1.doStartTag();
    if (_jspx_th_x_out_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_x_out_select_nobody.reuse(_jspx_th_x_out_1);
      return true;
    }
    _jspx_tagPool_x_out_select_nobody.reuse(_jspx_th_x_out_1);
    return false;
  }

  private boolean _jspx_meth_x_out_2(javax.servlet.jsp.tagext.JspTag _jspx_th_x_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_x_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  x:out
    org.apache.taglibs.standard.tag.rt.xml.ExprTag _jspx_th_x_out_2 = (org.apache.taglibs.standard.tag.rt.xml.ExprTag) _jspx_tagPool_x_out_select_nobody.get(org.apache.taglibs.standard.tag.rt.xml.ExprTag.class);
    _jspx_th_x_out_2.setPageContext(_jspx_page_context);
    _jspx_th_x_out_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_x_forEach_0);
    _jspx_th_x_out_2.setSelect("$item/title");
    int _jspx_eval_x_out_2 = _jspx_th_x_out_2.doStartTag();
    if (_jspx_th_x_out_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_x_out_select_nobody.reuse(_jspx_th_x_out_2);
      return true;
    }
    _jspx_tagPool_x_out_select_nobody.reuse(_jspx_th_x_out_2);
    return false;
  }
}
